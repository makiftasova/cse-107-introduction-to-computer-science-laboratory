;Mehmet Akif TAŞOVA <makiftasova@gmail.com>
; Gebze Institute of Technology Computer Science Department
; CSE 107 Practice
; Writing alphabet to screen (masking example)

LOAD R3, 01010101b
LOAD R0, 00000001b
LOAD R2, 1

AND R4, R3, R0

LOAD RF, 10
LOAD RF, 10
LOAD RF, 10
LOAD RF, 10

	
jmpEQ R4 = R0, Upper
jmp Lower

Lower:
	LOAD R1, [FirstLower]
	LOAD R0, [LastLower]
	ADDI R0, R0, R2
	jmp Print

Upper:
	LOAD R1, [FirstUpper]
	LOAD R0, [LastUpper]
	ADDI R0, R0, R2
	jmp Print

Print:
	

	MOVE RF, R1
	ADDI R1, R1, R2
	jmpEQ R1 = R0, END
	jmp Print

END:
	HALT

FirstUpper: db "A"
LastUpper: db "Z"
FirstLower: db "a"
LastLower: db "z"
