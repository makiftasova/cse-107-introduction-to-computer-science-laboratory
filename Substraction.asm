;Mehmet Akif TAŞOVA <makiftasova@gmail.com>
; Gebze Institute of Technology Computer Science Department
; CSE 107 Assignment
;  Substraction

LOAD RD, 8; Assingned integer value 8 to RegisterD
LOAD RC, 3  ; Assingned integer value 3 to RegisterC
LOAD RB, 2  ; Assingned integer value 2 to RegisterB
LOAD RA, 48  ; add value for convert ASCII code to decimal number
LOAD R9, 255 ;Value for XOR Operation (using in Two's Complement)
LOAD R8, 1 ;Value for Two's Complement

STORE RD, [90h]
STORE RC, [91h]
STORE RB, [92h]
STORE RA, [93h]
STORE R9, [94h]
STORE R8, [95h]

LOAD R1, [90h]  ; Equals value of RegisterD(8)
LOAD R2, [91h] ; Equals value of RegisterC(3)
LOAD R3, [92h] ; Equals value of RegisterB(2)
LOAD R4, [93h] ; Equals value of RegisterA(48)

LOAD R5, [94h] ; Equals value of Register9(255)
LOAD R6, [95h] ; Equals value of Register8(1)

XOR R2,R2,R5 ;Two's Complement for value 3 (Stored on Register2 and Memory Cell 91h)
ADDI R2,R2,R6 ;Two's Complement for value 3 (Stored on Register2 and Memory Cell 91h)

XOR R3,R3,R5 ;Two's Complement for value 2 (Stored on Register3 and Memory Cell 92h)
ADDI R3,R3,R6 ;Two's Complement for value 2 (Stored on Register3 and Memory Cell 92h)

ADDI R1,R1,R2 ;It equals 8-3
ADDI R1,R1,R3 ;It Equals (8-3)-2
ADDI R1,R1,R4 ;It converts ASCII value to decimal number

LOAD RF,10 ;Print a clean line
LOAD RF,10 ;Print a clean line
LOAD RF,10 ;Print a clean line
LOAD RF,10 ;Print a clean line

MOVE RF,R1 ; Move value of Register1 to RegisterF for print value of R1 to screen

HALT ;Stop the execution of program
