;Mehmet Akif TAŞOVA <makiftasova@gmail.com>
; Gebze Institute of Technology Computer Science Department

LOAD R0,9 ;"Counter End"
LOAD R1,0 ;"Counter Start"

LOAD R2,0 ;"Value start"
LOAD R3,1 ;"increase constant"

LOAD R4,48 ;"Constant for  DecimalToASCII"

LOAD R5,0 ;"Garbage Collector"

Nums: ADDI RF,R5,R4 ;"Su Values at R5 and R4 and assng the sum to R0"
	ADDI R5,R5,R3 ;"Add 1 to Return Value"
	ADDI R1,R1,R3 ;"Add 1 to Counter"
	JMPLE R1<=R0,Nums ;"Loop While R1<=R0"
	
