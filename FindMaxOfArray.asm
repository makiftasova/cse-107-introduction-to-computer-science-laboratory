;Mehmet Akif TAŞOVA <makiftasova@gmail.com>
; Gebze Institute of Technology Computer Science Department
; CSE 107 Assignment
;  Find max of array

LOAD R1, 8
LOAD R2, 5
LOAD R3, 4
LOAD R4, 9

LOAD RE,48 ;for Convert Decimal numbers to ASCII code

LOAD RF,10
LOAD RF,10
LOAD RF,10
LOAD RF,10


R1LER2: MOVE R0, R2			;Check is R1 Lower or Equal R2
	jmpLE R1 <= R0, R2LER3	;if R2 >= R1 then jump to R2LER3 else jump to R1LER3
	jmp R1LER3

R2LER3: MOVE R0, R3			;Check is R2 Lower or Equal R3
	jmpLE R2 <= R0, R3LER4	;if R3 >= R2 then jump to R3LER4 else jump to R2LER4
	jmp R2LER4

R1LER3: MOVE R0, R3			;Check is R1 Lower or Equal R3
	jmpLE R1 <= R0, R3LER4	;if R3 >= R1 then jump to R3LER4 else jump to R1LER4
	jmp R1LER4

R3LER4: MOVE R0, R4			;Check is R3 Lower or Equal R4
	jmpLE R3 <= R0, PrintR4	;if R4 >= R3 then jump to PrintR4 else jump to PrintR3
	jmp PrintR3

R1LER4: MOVE R0, R4			;Check is R1 Lower or Equal R4
	jmpLE R1 <= R0, PrintR4	;if R2 >= R1 then jump to PrintR4 else jump to PrintR1
	jmp PrintR1

R2LER4: MOVE R0, R4			;Check is R2 Lower or Equal R4
	jmpLE R2 <= R0, PrintR4	;if R2 >= R1 then jump to PrintR4 else jump to PrintR2
	jmp PrintR2

PrintR1:	ADDI R1,R1,RE  ;Print value of Register1
	MOVE RF,R1
	jmp END

PrintR2:	ADDI R2,R2,RE ;Print value of Register2
	MOVE RF,R2
	jmp END
	
PrintR3:ADDI R3,R3,RE ;Print value of Register3
	MOVE RF,R3
	jmp END

PrintR4:	ADDI R4,R4,RE ;Print value of Register4
	MOVE RF,R4
	jmp END

END:	HALT
