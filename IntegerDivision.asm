;Mehmet Akif TAŞOVA <makiftasova@gmail.com>
; Gebze Institute of Technology Computer Science Department
; CSE 107 Assignment
;  Integer Division


LOAD R1, 80 ; Integer to divide
LOAD R2, 2 ;Divider
LOAD R3, 48 ; Constant for ASCII to Decimal number convertation
LOAD R4, 1
LOAD R5, 255
LOAD R6, 0 ;value of  result

;For Print two digit results
LOAD RA, 10
LOAD RB, 0
XOR RA, RA, R5
ADDI RA, RA, R4
;Additional part ends

LOAD RE, ZeroDivision
LOAD RD, OutOfRange
LOAD RC, 1
LOAD R0, 0 

LOAD RF,10
LOAD RF,10
LOAD RF,10
LOAD RF,10

jmpEQ R2 = R0, ZeroDivError

MOVE R7,R2

TwosComplement:	XOR R2,R2,R5
			ADDI R2,R2,R4


DIVIDE:	ADDI R1,R1,R2
	ADDI R6,R6,R4
	MOVE R0,R1
	jmpLE R7 <= R0, DIVIDE
	jmp OverflowCheck


PRINT:	
	ADDI R6,R6,R3
	ADDI RB, RB, R3
	LOAD R0, 57
	jmp PREPARE

PREPARE:	jmpLE R6 <= R0, PRINT_HALT
		ADDI R6,R6,RA
		ADDI RB, RB, R4
		jmp PREPARE

PRINT_HALT:	MOVE RF, RB
		MOVE RF, R6
		HALT

OverflowCheck: LOAD R0,99
		jmpLE R6 <= R0, PRINT
		jmp ScreenOverflow

ZeroDivError:	
		LOAD R0, 0 ; Using for string-terminator		
		LOAD RF,[RE]
		ADDI RE,RE,RC
		jmpEQ RF = R0, END
		jmp ZeroDivError

ScreenOverflow:	
		LOAD R0, 0 ; Using for string-terminator
		LOAD RF,[RD]
		ADDI RD,RD,RC
		jmpEQ RF = R0, END
		jmp ScreenOverflow

ZeroDivision: db "err: division by zero",0

OutOfRange: db "wrn: cannot show the result",0

END: HALT
