;Mehmet Akif TAŞOVA <makiftasova@gmail.com>
; Gebze Institute of Technology Computer Science Department
; CSE 107 Practice
; STORE example and sum of integers

Load R0,06 ;"write value 06 to Register0"
Load R1,03 ;"write value 03 to Register1"

Store R0,[20h]	;"Store value of Register0 to MemoryCell20"
Store R1,[21h]	;"Store value of Register0 to MemoryCell21"

Load R2,[20h]	;"Load value of MemoryCell20(Hex) to Register2"
Load R3,[21h]	;"Load value of MemoryCell21(Hex) to Register3"

Addi R4,R2,R3	;"sum valufe of Regiter2 and Register3 and write the um to Register4"

Store R4,[22h]	;"Store value of Register4 to MemoryCell22(Hex)"

load R5,[22h]	;"Load value of MemoryCell22(Hex) to Register5"

load R6,48		;"write value 06 to Register6"

addi RF,R5,R6	;"Translaet value to ASCII and write it to FegisterF"
