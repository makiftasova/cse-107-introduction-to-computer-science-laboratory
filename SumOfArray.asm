;Mehmet Akif TAŞOVA <makiftasova@gmail.com>
; Gebze Institute of Technology Computer Science Department
; CSE 107 Assignment
;  Sum of Array

LOAD R1, ARRAY ; LOAD Array's first number to Register1
LOAD R2, 1 ; Counter increaser
LOAD R0, 0 ; array-terminator
LOAD R4, 0 ; Sum value
LOAD R5, 48 ; Value for converting ASCII to Decimal and Decimal toASCII
LOAD R6, 255 ; Value ofr calcualting Two's Complement

;For printing two digit results
LOAD R9, 10
LOAD RC, 0

XOR RA, R9, R6
ADDI RA, RA, R2 ; value of RegisterA is equals -10 after here



TwosComplement:	XOR R7, R5, R6
			ADDI R7, R7, R2

CleanScreen:	LOAD RF, 10
		LOAD RF, 10
		LOAD RF, 10
		LOAD RF, 10

;Loop for calculating sum of array's elements
Sum:	LOAD R3, [R1]			; LOAD value MemoryCell adress which palced in Register1 to Register3
	jmpEQ R3 = R0, SelectMethod		; IF R0 equals to zero(0) THEN Print the value of Register4 ELSE continue to sum proces
	ADDI R3, R3, R7		; Sum the value of Register3 with -48 and place it in Register3
	ADDI R1, R1, R2		; Increase the counter
	ADDI R4, R4, R3		;sum value of Register4 with value of Register3
	jmp Sum			; JUMP back to label SUM




;Declaration of PRINT function
SelectMethod:	ADDI R4, R4, R5
		ADDI RC, RC, R5
		LOAD R0, 57

Prepare:jmpLE R4 <= R0, JustPrint
	ADDI R4, R4, RA
	ADDI RC, RC, R2
	jmp Prepare

JustPrint:	MOVE RF, RC
		MOVE RF, R4
		HALT ;Stop the executing

; Definition of Array
ARRAY: db "15648",0
